# forwardemail.net helper

[![Get the Add-on](https://addons.cdn.mozilla.net/static/img/addons-buttons/TB-AMO-button_2.png)](https://addons.thunderbird.net/en-US/thunderbird/addon/forwardemail-helper/)

Displays a custom “Correspondents” column that replaces its value with the Reply-To header if the email comes from no-reply@forwardemail.net.

## Usage

1. Install the add-on [from Thunderbird Add-ons](https://addons.thunderbird.net/en-US/thunderbird/addon/forwardemail-helper/)
2. Disable the built-in “Correspondents” column and move the new one in its place
